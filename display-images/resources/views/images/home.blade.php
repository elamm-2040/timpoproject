@extends('layouts.general')

@section('content')

    <div id="upload">
        <h3>Upload and Display Images</h3>

        <div class="row">
            <div class="progress" v-if="loading">
                <div class="indeterminate"></div>
            </div>
            <form class="col s12"
                  data-parsley-validate
                  method="post"
                  v-on:submit.prevent="submit"
                  autocomplete="off">
                <div class="row valign-wrapper">
                    <div class="file-field input-field col s8">
                        <div class="btn blue lighten-1">
                        <span>
                            File
                            <i class="material-icons right">attach_file</i>
                        </span>
                            <input type="file"
                                   accept="image/png"
                                   v-on:change="validateForm"
                                   id="inputFile">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate"
                                   type="text"
                                   id="inputPath">
                        </div>
                    </div>
                    <div class="col s2">
                        <button class="btn waves-effect waves-light col s12  blue-grey"
                                type="button"
                                v-on:click="clear">
                            Clear
                            <i class="material-icons right">block</i>
                        </button>
                    </div>
                    <div class="col s2">
                        <button class="btn waves-effect waves-light col s12 green lighten-1"
                                type="submit"
                                :disabled="enableSend">
                            Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </form>

            <div class="col s7 rcorner z-depth-2">
                <img class="responsive-img" :src="path">
            </div>
            <div class="col s4 offset-s1 rcorner scroll z-depth-2">
                <a class="cursor" v-on:click="imgSelected(item)" v-for="item in list">
                    <img class="responsive-img" :src="item.url">
                    <div class="divider"></div>
                </a>
            </div>
        </div>
    </div>

@endsection
