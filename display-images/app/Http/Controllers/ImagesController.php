<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    private $target_url;

    public function __construct()
    {
        $this->target_url = 'https://test.rxflodev.com';
    }

    public function index()
    {
        return view('images.home');
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'base64' => 'required'
        ]);

        $base64 = ["imageData" => $request->base64];
        return response()->json($this->send_img($base64));
    }

    private function send_img($base_64)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->target_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $base_64);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $result = ["error" => curl_error($ch)];
        }

        curl_close($ch);

        return $result;
    }
}
