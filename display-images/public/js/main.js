new Vue({
    el: '#upload',
    data: {
        path: '',
        enableSend: true,
        base64: '',
        toast: Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3500,
            timerProgressBar: true
        }),
        loading: false,
        list: []
    },
    mounted: function () {
        this.loading = true;
        if (localStorage.getItem('arrImgs')) {
            this.list = JSON.parse(localStorage.getItem('arrImgs'));
        } else {
            localStorage.setItem('arrImgs', JSON.stringify(this.list));
        }

        window.addEventListener("load", () => {
            this.loading = false;
        });
    },
    methods: {
        clear: function () {
            this.path = '';
            $('#inputPath').val('');
            $('#inputFile').val('');
            this.enableSend = true;
        },
        validateForm: function (event) {
            this.loading = true;

            if (typeof event.target.files !== 'undefined') {
                const file = event.target.files[0];

                if (file.type === 'image/png') {
                    this.enableSend = false;

                    let reader = new FileReader();
                    reader.readAsDataURL(file);

                    this.imgProcess(reader)
                        .then(result => {
                            this.base64 = result.split('data:image/png;base64,')[1];
                            this.loading = false;
                        })
                        .catch(function (e) {
                            this.toast.fire({
                                icon: 'error',
                                title: 'Something was wrong'
                            });
                            console.error(e);
                            this.loading = false;
                        });
                } else {
                    this.toast.fire({
                        icon: 'error',
                        title: 'This file is not a PNG image'
                    });
                    this.loading = false;
                }
            } else {
                this.toast.fire({
                    icon: 'error',
                    title: 'No file charged'
                });
                this.loading = false;
            }
        },
        imgProcess: function (reader) {
            return new Promise(resolve => {
                reader.onload = (_event) => {
                    this.path = reader.result;
                    resolve(_event.target.result);
                }
            });
        },
        submit: function () {
            this.loading = true;
            axios({
                    method: 'post',
                    url: '/api/upload-img',
                    data: {
                        base64: this.base64
                    }
                }
            ).then(response => {
                const data = JSON.parse(response.data);

                if (typeof data.error === 'undefined') {
                    if (data.status === 'success') {
                        this.clear();
                        this.toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        this.list.unshift(data);
                        this.path = data.url;
                        localStorage.setItem('arrImgs', JSON.stringify(this.list));
                    } else {
                        this.toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                } else {
                    this.toast.fire({
                        icon: 'error',
                        title: 'Something was wrong'
                    });
                    console.error(data.error);
                }
                this.loading = false;
            }).catch(error => {
                this.toast.fire({
                    icon: 'error',
                    title: 'Something was wrong'
                });
                console.error(error);
                this.loading = false;
            })
        },
        imgSelected: function (item) {
            this.path = item.url;
        }
    }
})
